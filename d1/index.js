console.log("hello ")


/*

	Functions are lines/block f codes that tell our devices/application to perform certain tasks when called/invoked
 
*/
// function printInput(){
// 	let nickname = prompt("What is your nickname? ")
// 	console.log("Hello " + nickname)
// }

// printInput()


// However, for some use cases, this may not be ideal. For other cases, function can also process data  directly passes into it instead of reling on global variables such as prompt();


// Consider this function:
// "name" is what we call a parameter

/*
	PARAMETER
		-It acts as named variable/container that exists only inside of a function;
		- it is used to store information that is provided to a function when it is call/invoked.
*/

function printName(name){
	console.log("Hello! " + name);
};

// "Ungart", the information provided directly into the function, is called an argument. Values passed  when invoking function called arguments. These arguments are then stored as the parameters within the function

printName("Ungart");
printName("Lublub");
printName("Jane");

let sampleVar = "Yua";
printName(sampleVar)

// multiple parameters in a function

function areNumbers(num1, num2){
	console.log("the numbers passed as arguments are: " + num1 + " and " + num2);
	console.log(num1);
	console.log(num2);
};

areNumbers(25, 56);

function myFriends(f1, f2, f3){
	console.log("My friends are "+ f1 +", " + f2 +", " + f3 +", ");
	
};

myFriends("Tony", "Natasha", "Steve")

function checkDivisibilityBy8(num){
	let remainder = num%8;
	console.log("The remainder of " + num + " is " + remainder);
	let isDivisibleBy8 = remainder ===0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(46);
// We can also do the same using prompt(). However, take not that using prompt() outputs a string data type.
// String data types are not ideal and cannot be used for mathematical computaions



function checkDivisibilityBy4(num){
	let remainder = num%4;
	console.log("The remainder of " + num + " is " + remainder);
	let isDivisibleBy4 = remainder === 0;
	console.log("Is " + num + " divisible by 4?");
	console.log(isDivisibleBy4);
};

checkDivisibilityBy4(14);


function isEven(num){
	console.log(num % 2 === 0); 
};

function isOdd(num1){
	console.log(num1%2 !== 0);
}

isEven(20);
isEven(21);
isOdd(30);
isOdd(31);


/*
	FUNCTION AS ARGUMENTS
		function parameters can also acceot other function as arguments
		some complex function use other funcstions as arguments to perform complicated task/result 

*/
function argumentFunction(){
	console.log("This function is passed into another function.");
};

function invokeFunction(functionParameter){
	functionParameter();
};

// adding and removing of the parenthesis impacts the output of JS heavily. when a function is used with parenthesis, it denotes that invokin/calling a function - that is why when we call a function, the functionParameter as an argument does not have any parenthesis. 

// if the function as parameter does not have parenthesis, the function has an argument should have parenthesis to denote that it is a function passed as an argument

invokeFunction(argumentFunction);


// let firstName = "Juan";
// let middleName = "Dela";
// let lastName = "Cruz";

// console.log(firstName + " " + middleName+ " " + lastName)


// Return Statement
// return statement allows us to output a value from a function to passed to the line/blocked  of code that invoked/called our function

function returnFullName(firstName, middleName, lastName){
	return firstName+" "+middleName+" "+lastName;
	console.log("This message will not be printed");
};

/*
	this creates a value based on the return statement in the returnFullName function
	returnFullName("Jeffrey", "Smith", "Bezos")
*/

console.log(returnFullName("Jeffrey", "Smith", "Bezos"));


// the value returned from a function can also be stored inside a variable
let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName)

function returnAddress(country, continent){
	return country+" "+continent;
};

console.log(returnAddress("Philippines", "Southeast Asia"));

function printPlayerInfo(username, level, job){
	console.log("Username: "+ username);
	console.log("Level: " + level);
	console.log("Job: " + job);
}

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);